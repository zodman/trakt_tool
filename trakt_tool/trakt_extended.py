import trakt
import trakt.errors
import trakt.movies
import trakt.core
import trakt.tv
import click
from tmdb3 import set_key
import tmdb3
import pytvdbapi.api
from mal_api import search_mal
from yandex_translate import YandexTranslate
from kn3 import Kn3
import os
import yaml
import sys
import logging
import anidb
import traceback
log = logging.getLogger(__name__)

HOMEPATH=os.environ.get("HOME", "/root/")

config = yaml.load(open(os.path.join(HOMEPATH, ".trakt_toolrc.yaml")).read())

TMDB_API_KEY = config.get("tmdb_api_key")
TVDB_APIKEY = config.get("tvdb_api_key")
translate = YandexTranslate(config.get("yandex_api_key"))
set_key(TMDB_API_KEY)
trakt.core.AUTH_METHOD = trakt.core.OAUTH_AUTH


def init_trakt():
    CLIENT_ID = config.get("trakt_client_id")
    CLIENT_SECRET = config.get("trakt_client_secret")
    trakt.init(config.get("trakt_username"), client_id=CLIENT_ID, client_secret=CLIENT_SECRET,
                         store=True)


@click.command()
@click.argument("name")
@click.argument("name_track")
@click.option("--is-anime", is_flag=True, default=True)
def main(name,name_track,is_anime):
    name_search = u" ".join(name)
    r = gen_api(name_track, is_anime, name_search)
    import pprint
    pprint.pprint(r)


def trans(text):
    t = text
    if t:
        return translate.translate(t, "es").get("text").pop()
    return ''


def gen_api(name, is_anime, anime_search, mal_id=False):
    result = {}
    name_search = name
    trakt_dict, tmdb_dict, mal_dict, tvdb_dict, anidb_dict = {}, {}, {}, {}, {}

    try:
        trakt_result = trakt.tv.TVShow(name_search)
        is_movie = False
    except:
        traceback.print_exc()
        trakt_result = trakt.movies.Movie(name_search)
        is_movie = True
    log.info("gen api")
    print(trakt_result)
    overview = trans(trakt_result.overview)
    genres = [trans(i) for i in trakt_result.genres]
    alias = [{'title': i.title, 'country': i.country} for i in trakt_result.aliases]
    comments = [{'text': trans(i.comment), 'username': i.user.username,
                'name': getattr(i.user, "name", ""), 'raiting': i.user_rating} 
                for i in trakt_result.comments]
    person = []
    for i in trakt_result.people:
        try:
            img = i.images.get("headshot",{}).get("thumb",False)
        except trakt.errors.NotFoundException:
            traceback.print_exc()
            continue
        if img:
            img = img.replace("walter.trakt.us", "walter.trakt.tv")
            person.append({
                    'image': img, #Kn3.import_to_kn3(img),
                    'name':i.name,
                })
    status = getattr(trakt_result,"status", "Terminado")

    trakt_dict.update(dict(title=trakt_result.title, overview=overview,
        year=trakt_result.year, alias=alias,
        genres_en = trakt_result.genres,
        status=status, runtime=trakt_result.runtime,
        genres=genres, rating=trakt_result.rating,
        votes=trakt_result.votes, ids=trakt_result.ids["ids"],
        ratings=trakt_result.ratings, people=person,
        comments=comments,
    ))
    # print trakt_result
    tmpdb_id = trakt_result.ids["ids"].get("tmdb")
    #print trakt_result.ids
    # print tmpdb_id
    if tmpdb_id:
        if is_movie:
            tmdb_result = tmdb3.Movie(tmpdb_id)
        else:
            tmdb_result = tmdb3.Series(tmpdb_id)
        if hasattr(tmdb_result, "youtube_trailers"):
            youtube_trailers = [i.geturl() for i in tmdb_result.youtube_trailers ]
        else:
            youtube_trailers = []
        try:
            poster_images = [i.geturl() for i in tmdb_result.posters ]
            backdrops = [i.geturl() for i in tmdb_result.backdrops ]
            overview = tmdb_result.overview
            tmdb_dict.update(dict(trailers=youtube_trailers,
                             backdrops=backdrops, posters=poster_images,
                             votes=tmdb_result.votes, popularity=tmdb_result.popularity))
        except KeyError:
            pass


    tvdb_id = trakt_result.ids["ids"].get("tvdb")
    if tvdb_id:
        tvdb_api = pytvdbapi.api.TVDB(TVDB_APIKEY, banners=True)
        tvdb_result = tvdb_api.get_series(tvdb_id, "en")
        fanarts = []
        posters = []
        seasons = []
        for i in tvdb_result.banner_objects:

            if i.BannerType.lower() == "fanart":
                fanarts.append(i.banner_url)
            elif i.BannerType.lower() == "poster":
                posters.append(i.banner_url)
            elif i.BannerType.lower() == "season":
                seasons.append({'number': i.Season, 'image': i.banner_url})
        tvdb_dict.update(dict(fanarts=fanarts, posters=posters, seasons=seasons))
    
    if is_anime:
        mal_result = search_mal(anime_search, mal_id)
        mal_dict.update(mal_result)
        anime_api = anidb.AniDB('nekoanimedd', 1)



    d = dict(trakt=trakt_dict, tvdb=tvdb_dict, tmdb=tmdb_dict, mal=mal_dict,
             type=is_movie, anidb = anidb_dict)
    #print d["tmdb"]
    return d

if __name__ == "__main__":
    main()
