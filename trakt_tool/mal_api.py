# encoding=utf8
import requests
from webscraping import xpath

import urllib2
from urllib import urlencode, quote
import datetime
import HTMLParser


USR="zodman1"
PWSS="zxczxc"
URL = "https://myanimelist.net/api/"

auth = (USR,PWSS)
headers = {'User-Agent':'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36'}
cookies = {"incap_ses_224_81958":"P6tYbUr7VH9V6shgudAbA1g5FVYAAAAAyt7eDF9npLc6I7roc0UIEQ=="}


def translate(to_translate, to_langage="auto", langage="auto"):
    agents = {'User-Agent':"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)"}
    before_trans = 'class="t0">'
#    link = "http://translate.google.com/m?hl=%s&sl=%s&q=%s" % (to_langage, langage, to_translate.replace(" ", "+").replace("\n",""))
    link = "http://translate.google.com/m?"
    h = HTMLParser.HTMLParser()
    to_t = to_translate.decode("unicode-escape")
    to_trans = h.unescape(to_t)
    params = dict(hl=to_langage,sl=langage,q=to_trans)
    data = urlencode(dict([k, v.encode('utf-8')] for k, v in params.items()))
    request = urllib2.Request(link+data, headers=agents)
    page = urllib2.urlopen(request).read()
    result = page[page.find(before_trans)+len(before_trans):]
    result = result.split("<")[0]
    result.replace("d>","")
    return result

def search_mal(anime_str, mal_id=False):
    from trakt_extended import trans
    resp = requests.get("https://api.jikan.moe/v3/anime/{}/".format(mal_id))     
    j = resp.json()
    assert False, j
    mal_id = j.get("mal_id")
    title = j.get("title")
    title_en = j.get("title_english")
    synonyms = j.get("title_synonyms")
    status = j.get("status")
    img = j.get("image_url")
    synopsys = j.get("synopsis")
    episodes = j.get("episodes")
    resumen = synopsys
    type_ = j.get("type")

    if status == u"Emisión terminada":
        status = "Completa"
    if status == "Transmitido Terminado":
        status = "Completa"

    if status == "Actualmente Transmitido":
        status = u"En emisión"


    if synonyms == "" or not synonyms:
        synonyms = title


    d = {'id': mal_id, 'title':title, 'title_en':title_en, 'synonyms': synonyms,
            'status':status, 'synopsis':trans(synopsys), 'image':img, 'episodes':episodes,
            'resumen':trans(resumen), 'type':type_,}
    d.update(j)
    return d 
def i__search_mal(anime_str, mal_id = False):
    from .trakt_extended import trans

    response = requests.get(URL + "anime/search.xml?q="+ anime_str,auth=auth)
    content = response.content
    #$print response.url
    assert response.status_code == 200, "MAL NOT FOUND %s" % response.status_code
    if not mal_id is False:
         for e in xpath.search(content,"//entry"):
             if mal_id in e.decode("utf8"):
                 content = e
                 break

    mal_id = xpath.get(content, "//id")
    title = xpath.get(content, "//title").decode("utf-8")
    title_en = xpath.get(content, "//english")
    type_ = xpath.get(content, "//type")
    synonyms = xpath.get(content, "//synonyms")
    status = xpath.get(content, "//status")
    synopsys = xpath.get(content, "//synopsis")
    img  = xpath.get(content, "//image")
    episodes = xpath.get(content,"//episodes")
    resumen = synopsys.replace("&lt;br /&gt;", " ").replace("\n\r","")
    status = translate(status,'es').decode("utf8")
    if status == u"Emisión terminada":
        status = "Completa"
    if status == "Transmitido Terminado":
        status = "Completa"

    if status == "Actualmente Transmitido":
        status = u"En emisión"


    if synonyms == "" or not synonyms:
        synonyms = title
    return {'id': mal_id, 'title':title, 'title_en':title_en, 'synonyms': synonyms,
            'status':status, 'synopsis':trans(synopsys), 'image':img, 'episodes':episodes,
            'resumen':trans(resumen), 'type':type_,}
